package org.khmeracademy.repositoy;

import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.khmeracademy.models.category.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("SELECT \n" +
            "m.maincategoryid,\n" +
            "m.maincategoryname,\n" +
            "m.maincategorylogourl,\n" +
            "m.maincategoryorder,\n" +
            "m.bgimage,\n" +
            "m.color,\n" +
            "m.status,\n" +
            "m.description,\n" +
            "m.\"index\",\n" +
            "m.maincategoryname_kh,\n" +
            "m.description_kh,\n" +
            "m.sub_of\n" +
            "FROM findAllCategoryLevel1 m")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "url", column = "maincategorylogourl"),
            @Result(property = "order", column = "maincategoryorder"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
            @Result(property = "des_kh", column = "description_kh"),
            @Result(property = "subCategories", column = "maincategoryid", many = @Many(select = "findAllCategoryLevel2"))
    })
    public List<Category> findAllCategoryLevel1();

    @Select("SELECT \n" +
            "m.maincategoryid,\n" +
            "m.maincategoryname,\n" +
            "m.maincategorylogourl,\n" +
            "m.maincategoryorder,\n" +
            "m.bgimage,\n" +
            "m.color,\n" +
            "m.status,\n" +
            "m.description,\n" +
            "m.\"index\",\n" +
            "m.maincategoryname_kh,\n" +
            "m.description_kh,\n" +
            "m.sub_of\n" +
            "FROM tblmaincategory m where m.sub_of=#{maincategoryid}  ORDER BY index ASC")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "url", column = "maincategorylogourl"),
            @Result(property = "order", column = "maincategoryorder"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
            @Result(property = "des_kh", column = "description_kh"),
            @Result(property = "subCategories", column = "maincategoryid", many = @Many(select = "findAllCategoryLevel3"))
    })
    public List<Category> findAllCategoryLevel2(Integer maincategoryid);


    @Select("SELECT \n" +
            "m.maincategoryid,\n" +
            "m.maincategoryname,\n" +
            "m.maincategorylogourl,\n" +
            "m.maincategoryorder,\n" +
            "m.bgimage,\n" +
            "m.color,\n" +
            "m.status,\n" +
            "m.description,\n" +
            "m.\"index\",\n" +
            "m.maincategoryname_kh,\n" +
            "m.description_kh,\n" +
            "m.sub_of\n" +
            "FROM tblmaincategory m where m.sub_of=#{maincategoryid}  ORDER BY index ASC")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "url", column = "maincategorylogourl"),
            @Result(property = "order", column = "maincategoryorder"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
            @Result(property = "des_kh", column = "description_kh")
//            @Result(property = "subCategories", column = "maincategoryid", many = @Many(select = "findAllCategoryLevel3"))
    })
    public List<Category> findAllCategoryLevel3(Integer maincategoryid);
}
