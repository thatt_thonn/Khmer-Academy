package org.khmeracademy.controller;

import org.khmeracademy.models.category.Category;
import org.khmeracademy.services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CategoryController {
    private CategoryServices categoryServices;

    @Autowired
    public void setCategoryServices(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }

    @GetMapping("/hello")
    @ResponseBody
    public List<Category> findAll(){
        return categoryServices.findAllCategoryLevel1();
    }
}
