package org.khmeracademy.models.category;

import java.util.List;

public class Category {
    private String id;
    private String name;
    private String url;
    private Integer order;
    private String bgimage;
    private String color;
    private Boolean status;
    private String description;
    private Integer index;
    private String name_kh;
    private String des_kh;
    private Integer sub_of;
    private String encriptId;

    private List<Category> subCategories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getBgimage() {
        return bgimage;
    }

    public void setBgimage(String bgimage) {
        this.bgimage = bgimage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName_kh() {
        return name_kh;
    }

    public void setName_kh(String name_kh) {
        this.name_kh = name_kh;
    }

    public String getDes_kh() {
        return des_kh;
    }

    public void setDes_kh(String des_kh) {
        this.des_kh = des_kh;
    }

    public Integer getSub_of() {
        return sub_of;
    }

    public void setSub_of(Integer sub_of) {
        this.sub_of = sub_of;
    }

    public String getEncriptId() {
        return id;
    }

    public void setEncriptId(String id) {
        this.id = id;
    }

// get category level 2


    public List<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<Category> subCategories) {
        this.subCategories = subCategories;
    }
}
