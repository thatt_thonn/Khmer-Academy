package org.khmeracademy.services;

import org.khmeracademy.models.category.Category;
import org.khmeracademy.repositoy.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class CategoryServices {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> findAllCategoryLevel1(){
        return categoryRepository.findAllCategoryLevel1();
    }

//    public List<Category> findAllCategoryLevel2(){
//        return categoryRepository.findAllCategoryLevel2(0);
//    }
}
