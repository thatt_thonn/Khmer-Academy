package org.khmeracademy.webServices;

import io.swagger.annotations.Api;
import org.khmeracademy.models.category.Category;
import org.khmeracademy.services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Categories API", description = "Categories", tags = { "Categories API" })
public class CategoryWebService {

    private CategoryServices categoryServices;

    @Autowired
    public void setCategoryServices(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }

    @GetMapping("/findAllCategoryLevel1")
    public ResponseEntity<Map<String,Object>> findAllCategoryLevel1(){

        List<Category> categories=categoryServices.findAllCategoryLevel1();
        Map<String,Object> response=new HashMap<>();
        if(categories.isEmpty()){
            response.put("status","false");
            response.put("message","No category to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status","true");
        response.put("message","success");
        response.put("data",categories);
        return new ResponseEntity(response,HttpStatus.OK);
    }

//    @GetMapping("/findAllCategoryLevel2")
//    public ResponseEntity<Map<String, Object>> findAllCategoryLevel2(){
//        List<Category> categories=categoryServices.findAllCategoryLevel2();
//        Map<String,Object> response=new HashMap<>();
//        if(categories.isEmpty()){
//            response.put("status","false");
//            response.put("message","No content to see now..");
//            return new ResponseEntity(response,HttpStatus.NO_CONTENT);
//        }
//        response.put("status","true");
//        response.put("message","success");
//        response.put("data",categories);
//        return new ResponseEntity(response,HttpStatus.OK);
//    }
}
