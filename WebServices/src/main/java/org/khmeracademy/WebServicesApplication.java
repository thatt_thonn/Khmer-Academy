package org.khmeracademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class WebServicesApplication{
    public static void main(String[] args) {
        SpringApplication.run(WebServicesApplication.class,args);
    }
}
